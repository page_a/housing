class CreateAds < ActiveRecord::Migration[5.2]
  def change
    create_table :ads, &:timestamps

    add_column :ads, :title, :text
    add_column :ads, :price, :float
    add_column :ads, :rooms, :integer
    add_column :ads, :agency, :text
    add_column :ads, :url, :text
    add_column :ads, :availability, :text
    add_column :ads, :ad_updated_at, :datetime
    add_column :ads, :surface, :float
  end
end
