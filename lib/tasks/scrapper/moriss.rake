namespace :scrapper do
  task moriss: %i[environment] do
    ::Scrapper::Moriss.call(url: 'https://www.morissimmobilier.com/recherche-immobiliere-avancee/?advanced_city=&surface-min=0&nb-chambres-min=0&budget-max=5000000')
  end
end
