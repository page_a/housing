# README

## Test (Je reve d'une maison)

## Backend
I use the gem interactor. The idea is to get one service per website (SRP).
You can launch the scrapper through the command `be rake scrapper:moriss`
This will fetch all the data asked in the tests. I use the gem Nokogiri to get
the appropriate information on the website. I used active storage to manage picture
and attach it to the model.

## Database
I use Sqlite mainly for productivity. 

## Frontend
I used the gem react-rails: basically the idea is to use the templating
file of Rails to instantiate a React component. For example the `index` function
in the `AdController` will call `index.html.erb` to render something. This file will then
call the appropriate React component. This is also where we can inject variable that will
be available in the props of the component. I also used Bootstrap for the front-end.

## How to test it ?

### Run the migration
`be rake db:migrate`

### Run the scrapper
`be rake scrapper:moriss`

### Endpoints ?
- http://localhost:3000/ads
- http://localhost:3000/ads/:id
