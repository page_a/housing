class Ad extends React.Component {

  constructor(props) {
    super(props);
  }

  Ad() {
    const { ad } = this.props;
    if (ad) {
      return (
        <div className="col-md-5 ads__container">
          <div className="row">
            <div>
              <h1>{ad.title}</h1>
            </div>
          </div>
          <div className="row">
            <img src={ad.image} />
          </div>
          <div className="row">
            <div className="col-md-6">
              <div className="row">
                <p>{ad.availability}</p>
              </div>
              <div className="row">
                <p>{ad.rooms} Chambre(s)</p>
              </div>
            </div>
            <div className="col-md-6">
              <div className="row">
                <p>{ad.rooms} Room(s) - {ad.surface} m2</p>
              </div>
              <div className="row">
                <p>{ad.price} euro</p>
              </div>
            </div>
          </div>
        </div>
      )
    }
  }

  render() {
    return (
      <div>
        {this.Ad()}
      </div>
    )
  }
}