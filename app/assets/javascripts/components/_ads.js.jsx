class Ads extends React.Component {

  constructor(props) {
    super(props);
  }

  renderAds() {
    const { ads } = this.props;

    let items = [];
    for (let index = 0; index < ads.length; index = index + 2) {
      items.push(<Ad ad={this.props.ads[index]} />)
      items.push(<Ad ad={this.props.ads[index + 1]} />)
    }
    return (
      <div className="row">
        {items}
      </div>
    )
  }

  render() {
    return (
      <div className="container">
        {this.renderAds()}
      </div>
    );
  }
}