const Index = (props) => {
  return(
    <div className="all-ads__container">
      <h1>All Ads!</h1>
      <Ads ads={props.ads}/>
    </div>
  )
};
