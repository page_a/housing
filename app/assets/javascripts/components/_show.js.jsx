const Show = (props) => {
  const { ad } = props;
  return (
    <div>
      <div className="container">
        <div className="show__container">
          <div className="row">
            <div className="col-md-3"></div>
            <Ad ad={ad} />
          </div>
        </div>
      </div>
    </div>
  )
};
