const NotFound = (props) => {
  return(
    <div>
      <h1>No record found for this id!</h1>
    </div>
  )
};
