class AdsController < ApplicationController
  before_action :set_ad, only: [:show]
  before_action :get_ads, only: [:index]

  def index
  end

  def show
  end

  private

  def set_ad
    @ad = Ad.find(params[:id])
    @ad = @ad.as_json.merge(image: url_for(@ad.image))
  end

  def get_ads
    @ads = Ad.all.with_attached_image.map do |ad|
      ad.as_json.merge(image: url_for(ad.image))
    end
  end
end
