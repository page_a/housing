require 'open-uri'

module Scrapper
  class Moriss
    include Interactor

    def call
      context.fail!(error: 'Missing parameters') if context.url.nil?

      data = Nokogiri::HTML(open(context.url))
      ads = data.css('.listing_wrapper.property_unit_type4')
      ads.each do |ad|
        price = ad.css('.propery_price4_grid').text.strip.gsub(/[* €.]/, '')
        ad_name = ad.css('h4 a').text.strip

        rooms = ad.css('.property_listing_details4_grid_view .infobath_unit_type4')
        number_rooms = if rooms
                         rooms.text.strip.gsub!(/Chambres/, '')
                       else
                         -1
                       end
        area = ad.css('.property_listing_details4_grid_view .infosize_unit_type4')
        surface = if area
                    area.text.strip.gsub(/[ m2]/, '')
                  else
                    'unknown'
        end

        agency = ad.css('.property_agent_unit_type4 .property_agent_wrapper a').text.strip

        link = ad.at_css('.carousel-inner .item.active a').attributes['href'].value

        url_picture = ad.at_css('.carousel-inner .item.active a img').attributes['data-original'].value

        disponibility = ad.at_css('.status-wrapper .ribbon-inside').text

        ad = Ad.new(title: ad_name, price: price, rooms: number_rooms.to_i, agency: agency, url: link,
                    availability: disponibility, ad_updated_at: Time.current, surface: surface)

        begin
          file = download_remote_file(url_picture)
          picture_name = "#{Digest::SHA256.hexdigest(url_picture)}.png"
          ad.image.attach(io: file, filename: picture_name, content_type: 'image/jpg')
        rescue Exception => e
          puts e.message
        end

        ad.save!
      end
    end

    def download_remote_file(url)
      response = Net::HTTP.get_response(URI.parse(url))
      StringIO.new(response.body)
    end
  end
end
